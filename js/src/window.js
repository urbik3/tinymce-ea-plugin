window.tinymceEAPlugin = window.tinymceEAPlugin || {}
window.tinymceEAPlugin.pluginWindow = window.tinymceEAPlugin.pluginWindow || {}
window.tinymceEAPlugin.treeview = window.tinymceEAPlugin.treeview || {}
window.tinymceEAPlugin.component.window = window.tinymceEAPlugin.component.window || {}
window.tinymceEAPlugin.component.btn = window.tinymceEAPlugin.component.btn || {}

/*
* MANAGES WINDOW GUI AND ELEMENT INSERTION
* core window open fn
* "insertElementFactory" curryies into public "pluginWindow.insertElement"
* - insertion can happen from any place of plugin (ex. dragDrop module)
* */

;((plugin, treeview, pluginWindow, componentWindow, componentBtn) => {

    pluginWindow.buttons = {
        elementName: {
            "class": componentBtn.className+"--element-name",
            settings: undefined,
            btn: undefined
        },
        diagramName: {
            "class": componentBtn.className+"--diagram-name",
            settings: undefined,
            btn: undefined
        },
        diagramImage: {
            "class": componentBtn.className+"--diagram-image",
            settings: undefined,
            btn: undefined
        }
    }

    /*
    * OPEN NEW INSTANCE OF WINDOW
    * uses custom window component and custom btn component
    * */
    pluginWindow.treeviewWindow = false

    pluginWindow.open = editor => dom => {
        pluginWindow.insertElement = insertElementFactory(editor)
        pluginWindow.treeviewWindow = componentWindow.open(editor, {
            title: "Výběr prvku",
            content: getHTML(dom),
            buttons: createButtons(),
            height: 400,
            width: 620
        })
    }

    const getHTML = dom => {
        let html = document.createElement("div")
        html.appendChild(dom)
        return html.innerHTML
    }

    const createButtons = () => {
        pluginWindow.buttons.elementName.settings = {
            text: 'Vložit název elementu',
            classes: `${componentBtn.className} ${pluginWindow.buttons.elementName.class}`,
            disabled: true,
            onclick: pluginWindow.insertElement(false)
        }
        pluginWindow.buttons.elementName.btn = componentBtn.create(pluginWindow.buttons.elementName.settings)

        pluginWindow.buttons.diagramName.settings = {
            text: 'Vložit název diagramu',
            classes: `${componentBtn.className} ${pluginWindow.buttons.diagramName.class}`,
            disabled: true,
            onclick: pluginWindow.insertElement(false)
        }
        pluginWindow.buttons.diagramName.btn = componentBtn.create(pluginWindow.buttons.diagramName.settings)

        pluginWindow.buttons.diagramImage.settings = {
            text: 'Vložit obrázek diagramu',
            classes: `${componentBtn.className} ${pluginWindow.buttons.diagramImage.class}`,
            disabled: true,
            onclick: pluginWindow.insertElement(true)
        }
        pluginWindow.buttons.diagramImage.btn = componentBtn.create(pluginWindow.buttons.diagramImage.settings)

        return [
            pluginWindow.buttons.elementName.btn,
            pluginWindow.buttons.diagramName.btn,
            pluginWindow.buttons.diagramImage.btn
        ]
    }

    /*
     * ELEMENT INSERTION
     * */
    pluginWindow.insertElement = false

    const insertElementFactory = editor =>
        isImage => (e = false) => {
            if(!treeview.selectedItem)
                return

            if(e) e.preventDefault()

            let
                insertion,
                data = new FormData(),
                elementType = treeview.selectedItem.attributes["data-type"].value,
                GUID = treeview.selectedItem.attributes["data-guid"].value,
                name = treeview.selectedItem.attributes["data-name"].value
            data.append("GUID", GUID)

            // INSERTION TYPE (NAME/IMAGE)
            if(!isImage) {
                data.append("name", name)
                data.append("isImage", 0)
                insertion = `<span class="${plugin.elementClass} ea-type-${elementType}" data-guid="${GUID}" data-type="${elementType}">${name}</span>`
                insertion += " "
            } else {
                data.append("isImage", 1)
                insertion = `<div class="${plugin.elementClass} ea-type-image" data-guid="${GUID}" data-type="${elementType}"><img src="ea_synchronization/diagrams/${GUID}.png" /></div>`
                insertion += " "
            }

            // INSERTION POSITION
            editor.selection.collapse(false)
            let currentNode = editor.selection.getNode()
            if(currentNode.classList.contains(plugin.elementClass)) {
                let range = document.createRange(),
                    selection = editor.selection
                range.setEndAfter(currentNode)
                selection.setRng(range)
                insertion = " " + insertion
            }

            // WRITES FILE NAME/IMAGE PLACEHOLDER TO FILE
            fetch(plugin.url.phpController, {
                method: 'post',
                body: data
            })
                .then(() => editor.insertContent(insertion))
        }

    /*
    * ITEM HAS BEEN (UN)SELECTED
    * updates window gui
    * */
    pluginWindow.itemSelected = () => {
        Object.keys(pluginWindow.buttons)
            .map(attr => pluginWindow.buttons[attr].btn)
            .forEach(btn => btn.disabled(true))
        
        if(!!treeview.selectedItem) {
            if(treeview.selectedItem.hasAttributes()) {
                let activeButtons = (treeview.selectedItem.attributes["data-type"] && treeview.selectedItem.attributes["data-type"].value === "1") ?
                    [pluginWindow.buttons.diagramName, pluginWindow.buttons.diagramImage] :
                    [pluginWindow.buttons.elementName]

                activeButtons
                    .map(btn => btn.btn)
                    .forEach(btn => btn.disabled(false))
            }
        }
    }

    return pluginWindow
})(window.tinymceEAPlugin, window.tinymceEAPlugin.treeview, window.tinymceEAPlugin.pluginWindow, window.tinymceEAPlugin.component.window, window.tinymceEAPlugin.component.btn)