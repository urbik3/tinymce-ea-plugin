window.tinymceEAPlugin = window.tinymceEAPlugin || {}
window.tinymceEAPlugin.component = window.tinymceEAPlugin.component || {}
window.tinymceEAPlugin.component.btn = window.tinymceEAPlugin.component.btn || {}

/*
* BUTTON COMPONENT
* needs to be created then appended to dom
* can be disabled using public fn
* */

;((plugin, btn, component) => {

    btn.className = "ea-plugin-component-btn"

    btn.create = ({
        text = false,
        classes = false,
        disabled = false,
        onclick = false
    }) => {
        const el = plugin.tag("button", {
            "class": classes + " " + btn.className
        })

        if(text)
            el.innerText = text

        if(onclick)
            el.addEventListener("click", onclick)

        const btnClass = {
            el,
            disabled: on => on ? el.classList.add(btn.className+"--disabled") : el.classList.remove(btn.className+"--disabled"),
            appendTo: parent => parent.appendChild(el)
        }

        if(disabled)
            btnClass.disabled(true)
        
        return btnClass
    }
    
    return btn
})(window.tinymceEAPlugin, window.tinymceEAPlugin.component.btn, window.tinymceEAPlugin.component)