window.tinymceEAPlugin = window.tinymceEAPlugin || {}
window.tinymceEAPlugin.treeview = window.tinymceEAPlugin.treeview || {}
window.tinymceEAPlugin.treeview.dragDrop = window.tinymceEAPlugin.treeview.dragDrop || {}
window.tinymceEAPlugin.pluginWindow = window.tinymceEAPlugin.pluginWindow || {}

/*
* TREEVIEW COMPONENT
* exposes selectedItem variable (dom node)
* "getDOM" fn creates html content from EA_Editor_file.json
* "init" fn then initializes the component
* */

;((plugin, treeview, dragDrop, pluginWindow) => {

    const
        treeviewName = "tinymce-ea-plugin-treeview",
        treeviewSelector = "#"+treeviewName

    // SELECTED ITEM
    treeview.selectedItem = false

    /*
    * GET DOM
    * */
    treeview.getDOM = json => {
        const insertItem = (parent, item) => {
            // ITEM
            let hasChildren = !!item.children.length,
                attrs = {
                    "data-guid": item.GUID,
                    "data-name": item.name,
                    "data-type": item.type,
                    "class": "item item--type-"+item.type
                }
            attrs.class += hasChildren ? " item--has-children" : ""
            let itemTag = plugin.tag("div", attrs)

            // ITEM CONTENT
            itemTag.appendChild(plugin.tag("div", {"class": "wrap-icon"}))
            let itemCont = plugin.tag("div", {"class": "item-cont"})
            itemCont.appendChild(plugin.tag("span", {"class": "image"}))
            let name = plugin.tag("span", {"class": "name"})
            name.innerHTML = item.name
            itemCont.appendChild(name)
            itemTag.appendChild(itemCont)

            // CHILDREN
            if(hasChildren) {
                let childrenCont = plugin.tag("div", {"class": "children"})
                item.children
                    .reduce(insertItem, childrenCont)
                itemTag.appendChild(childrenCont)
            }

            parent.appendChild(itemTag)

            return parent
        }

        let treeviewDOM = json.reduce(insertItem, plugin.tag("div", {"id": treeviewName}))

        return treeviewDOM
    }

    /*
    * INIT
    * each item is or can be
    * - draggable
    * - selectable
    * - (un)wrapable
    * */
    treeview.init = editor => () => {
        const
            el = document.querySelector(treeviewSelector),
            find = Array.prototype.find,
            forEach = Array.prototype.forEach,
            unwrapClass = "item--unwrapped",
            selectClass = "item--selected",
            items = el.querySelectorAll(treeviewSelector + " .item")

        dragDrop.initDrag(editor)

        const activateItem = item => {
            const
                childrenCont = find.call(item.children, child => child.classList.contains("children")),
                itemActiveArea = find.call(item.children, child => child.classList.contains("item-cont")),
                wrapIcon = item.classList.contains("item--has-children") && find.call(item.children, child => child.classList.contains("wrap-icon"))

            // FIND CHILDREN
            if(childrenCont && childrenCont.children && childrenCont.children.length)
                forEach.call(childrenCont.children, activateItem)

            // DRAG
            itemActiveArea
                .addEventListener("mousedown", dragDrop.itemStartDrag(item))

            // SELECT
            itemActiveArea
                .addEventListener("click", e => {
                    const isSelected = item.classList.contains(selectClass)
                    forEach.call(items, i => i.classList.remove(selectClass))
                    if(isSelected) {
                        item.classList.remove(selectClass)
                        treeview.selectedItem = false
                        pluginWindow.itemSelected()
                    }
                    else {
                        item.classList.add(selectClass)
                        treeview.selectedItem = item
                        pluginWindow.itemSelected()
                    }
                })

            // UNWRAP
            if(wrapIcon)
                wrapIcon
                    .addEventListener("click", e => {
                        if(item.classList.contains(unwrapClass)) {
                            item.classList.remove(unwrapClass)
                            // forEach.call(item.querySelectorAll(treeviewSelector + " .item"), i => i.classList.remove(selectClass))
                        }
                        else
                            item.classList.add(unwrapClass)
                    })
        }

        forEach.call(el.children, activateItem)
    }

    return treeview
})(window.tinymceEAPlugin, window.tinymceEAPlugin.treeview, window.tinymceEAPlugin.treeview.dragDrop, window.tinymceEAPlugin.pluginWindow)