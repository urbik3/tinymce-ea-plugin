window.tinymceEAPlugin = window.tinymceEAPlugin || {}
window.tinymceEAPlugin.treeview = window.tinymceEAPlugin.treeview || {}
window.tinymceEAPlugin.treeview.dragDrop = window.tinymceEAPlugin.treeview.dragDrop || {}
window.tinymceEAPlugin.pluginWindow = window.tinymceEAPlugin.pluginWindow || {}

window.tinymceEAPlugin.component = window.tinymceEAPlugin.component || {}
window.tinymceEAPlugin.component.btn = window.tinymceEAPlugin.component.btn || {}
window.tinymceEAPlugin.component.window = window.tinymceEAPlugin.component.window || {}

/*
* TREEVIEW DRAGnDROP MODULE
* makes treeview item draggable
* dragging starts after some distance
* droping into tinymce editor content inserts element name OR shows custom window with name/image option
* */

;((plugin, dragDrop, treeview, pluginWindow, componentWindow, componentBtn) => {

    const itemDraggedPrototype = {
        e: false,
        selectedItem: false,
        draggedElement: false,
        isDragging: false,
        startDrag: false
    }
    let itemDragged = Object.assign({}, itemDraggedPrototype)

    /*
    * MODULE INIT
    * init on window and editor
    * - tinymce editor content is different document (iframe) - needs separate events
    * */
    dragDrop.initDrag = editor => {
        // MOUSE MOVE
        const winContext = {type: "window", obj: window},
            windowMoveFn = itemDrag(winContext)
        window.removeEventListener("mousemove", windowMoveFn)
        window.addEventListener("mousemove", windowMoveFn)
        const editorContext = {type: "editor", obj: editor},
            editorMoveFn = itemDrag(editorContext)
        editor.off("mousemove", editorMoveFn)
        editor.on("mousemove", editorMoveFn)

        // MOUSE UP/DROP
        window.removeEventListener("mouseup", resetDrag)
        window.addEventListener("mouseup", resetDrag)
        const dropFn = drop(editor)
        editor.off("mouseup", dropFn)
        editor.on("mouseup", dropFn)

        // RESET EVENTS
        pluginWindow.treeviewWindow.on("close", () => {
            window.removeEventListener("mouseup", resetDrag)
            window.removeEventListener("mousemove", windowMoveFn)
            editor.off("mousemove", editorMoveFn)
            editor.off("mouseup", dropFn)
        })
    }

    /*
    * ITEM INIT
    * */
    dragDrop.itemStartDrag = item => e => {
        e.preventDefault()
        itemDragged = Object.assign({},
            itemDraggedPrototype,
            {e, startDrag: true, selectedItem: item}
        )
    }

    /*
    * CORE FUNCTIONS
    * itemDrag
    * - dragging item through window and editor
    * - dragging of element starts after some distance
    * drop
    * - dropping item to editor
    * showDialog
    * - after dropping diagram to editor, another custom window appears with insert name/image option
    * resetDrag
    * - default reset all data
    * */
    const itemDrag = context => e => {
        if(!itemDragged.startDrag)
            return

        const distance = Math.max(Math.abs(itemDragged.e.clientX - e.clientX), Math.abs(itemDragged.e.clientY - e.clientY))
        if(!itemDragged.isDragging && distance > 20) {
            if(treeview.selectedItem !== itemDragged.selectedItem)
                triggerEvent(
                    Array.prototype.find.call(itemDragged.selectedItem.children, child => child.classList.contains("item-cont")),
                    "click"
                )

            itemDragged.isDragging = true

            let draggedElement = plugin.tag("div", {id: "tinymce-ea-plugin-dragged-item"})
            draggedElement.innerText = Array.prototype.find.call(itemDragged.selectedItem.children, child => child.classList.contains("item-cont")).innerText
            document.body.appendChild(draggedElement)
            itemDragged.draggedElement = document.querySelector("#tinymce-ea-plugin-dragged-item")
        }

        if(!itemDragged.isDragging)
            return

        e.preventDefault()

        let x = e.clientX + 10,
            y = e.clientY - 10 - itemDragged.draggedElement.getBoundingClientRect().height,
            coords
        if(context.type === "editor")
            coords = plugin.getCoords(context.obj.iframeElement)
        else {
            let scrollPos = plugin.getScrollPos(window)
            coords = {left: scrollPos.scrollLeft, top: scrollPos.scrollTop}
        }
        x += coords.left
        y += coords.top

        itemDragged.draggedElement.style.left = x + "px"
        itemDragged.draggedElement.style.top = y + "px"
    }

    /*
    * drop item to editor
    * insert item name
    * if item is of type diagram show window with name/image options
    * */
    const drop = editor => e => {
        if(!itemDragged.isDragging)
            return

        // ELEMENT IS DIAGRAM -> SHOW DIALOG
        if(treeview.selectedItem.hasAttributes() && treeview.selectedItem.attributes["data-type"] && treeview.selectedItem.attributes["data-type"].value === "1") {
            resetDrag(e, false)
            window.removeEventListener("mouseup", resetDrag)
            pluginWindow.treeviewWindow.hide()

            showDialog(e, editor)
                // INSERT
                .then(isImage => {
                    pluginWindow.insertElement(isImage)()
                    resetDrag(e)
                    pluginWindow.treeviewWindow.show()
                    window.addEventListener("mouseup", resetDrag)
                })
                // CLOSE WINDOW
                .catch(() => {
                    resetDrag(e)
                    pluginWindow.treeviewWindow.show()
                    window.addEventListener("mouseup", resetDrag)
                })
            return
        }

        // INSERT NON-DIAGRAM ELEMENT
        pluginWindow.insertElement(false)()
        resetDrag(e)
    }

    /*
    * creates new window with image/name option - two buttons
    * */
    const showDialog = (e, editor) =>
        new Promise((resolve, reject) => {
            const
                insertDiagramName = Object.assign({},
                    pluginWindow.buttons.diagramName.settings,
                    {disabled: false, onclick: () => {
                            resolve(false)
                            chooseTypeWindow.close()
                        }
                    }
                ),
                insertDiagramImage = Object.assign({},
                    pluginWindow.buttons.diagramImage.settings,
                    {disabled: false, onclick: () => {
                            resolve(true)
                            chooseTypeWindow.close()
                        }
                    }
                )

            let
                winContent = plugin.tag("div"),
                btnName = componentBtn.create(insertDiagramName),
                btnImage = componentBtn.create(insertDiagramImage)
            btnName.el.style.width = "100%"
            btnImage.el.style.width = "100%"
            btnName.appendTo(winContent)
            btnImage.appendTo(winContent)
            const chooseTypeWindow = componentWindow.open(editor, {
                title: 'Typ vložení',
                content: winContent,
                width: 250,
                height: 60
            })

            chooseTypeWindow.on("close", reject)
        })

    /*
    * default behaviour resets all data
    * resetData = false resets only drag data, selectedItem remains
    * */
    const resetDrag = (e, resetData = true) => {
        if(itemDragged.isDragging)
            itemDragged.draggedElement.parentNode.removeChild(itemDragged.draggedElement)

        if(resetData)
            itemDragged = Object.assign({}, itemDraggedPrototype)
        else {
            itemDragged.startDrag = false
            itemDragged.isDragging = false
            itemDragged.draggedElement = false
        }
    }

    /*
    * UTILS
    * */
    const triggerEvent = (el, type) => {
        if ('createEvent' in document) {
            // modern browsers, IE9+
            var e = document.createEvent('HTMLEvents');
            e.initEvent(type, false, true);
            el.dispatchEvent(e);
        } else {
            // IE 8
            var e = document.createEventObject();
            e.eventType = type;
            el.fireEvent('on'+e.eventType, e);
        }
    }

    return dragDrop
})(window.tinymceEAPlugin, window.tinymceEAPlugin.treeview.dragDrop, window.tinymceEAPlugin.treeview, window.tinymceEAPlugin.pluginWindow, window.tinymceEAPlugin.component.window, window.tinymceEAPlugin.component.btn)