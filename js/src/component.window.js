window.tinymceEAPlugin = window.tinymceEAPlugin || {}
window.tinymceEAPlugin.component = window.tinymceEAPlugin.component || {}
window.tinymceEAPlugin.component.window = window.tinymceEAPlugin.component.window || {}

/*
* WINDOW COMPONENT
* has top panel, content, bottom buttons panel
* can close, hide, show, has close event, can be centered and dragged by top panel
* see "windowObject"
* */

;((plugin, componentWindow, component) => {

    componentWindow.className = "ea-plugin-component-window"

    componentWindow.open = (editor, {
        title = false,
        content = false,
        buttons = false,

        height = false,
        width = false
    }) => {
        const windowEl = plugin.tag("div", {
            "class": componentWindow.className
        })
        windowEl.innerHTML = document.getElementById("window-el").innerHTML

        /*
        * WINDOW OBJECT
        * */
        const windowObject = {
            // ATTRS
            el: windowEl,

            // METHODS
            close: () => {
                windowObject.events.close.forEach(cb => cb())
                windowEl.remove()
            },
            hide: () => windowEl.classList.add(componentWindow.className+"--hidden"),
            show: () => windowEl.classList.remove(componentWindow.className+"--hidden"),
            on: (type, fn) => {
                if(windowObject.events[type])
                    windowObject.events[type].push(fn)
            },
            centerPosition: componentWindow.centerPosition(containerEl),

            // INTERNAL
            events: {
                close: []
            },
            isDragging: false,
            mouseToContainerDragPos: {x: false, y: false}
        }

        // WINDOW PARTS
        const
            containerEl = windowEl.querySelector(".window-container"),
            panelEl = windowEl.querySelector(".top-panel"),
            titleEl = windowEl.querySelector(".title"),
            closeEl = windowEl.querySelector(".close-btn"),
            contentEl = windowEl.querySelector(".content"),
            buttonsEl = windowEl.querySelector(".buttons")

        // WINDOW CONTENT SETUP
        if(title)
            titleEl.innerText = title

        if(content) {
            if(typeof content === "string") {
                contentEl.innerHTML = content
            } else {
                contentEl.appendChild(content)
            }
        }

        // ADD BUTTONS
        if(buttons) {
            containerEl.classList.add("window-container--has-buttons")

            if(typeof buttons === "string") {
                buttonsEl.innerHTML = buttons
            } else {
                Array.prototype.forEach.call(buttons, btn => btn.appendTo(buttonsEl))
            }
        }

        // SET DIMENSIONS
        if(height || width) {
            if(height) {
                contentEl.style.height = height+"px"
                containerEl.style.height = "auto";
            }
            if(width) {
                contentEl.style.width = width+"px"
                containerEl.style.width = "auto";
            }
        }

        initDrag(editor, windowObject, containerEl, panelEl)

        window.document.body.appendChild(windowEl)
        componentWindow.centerPosition(containerEl)()

        closeEl.addEventListener("mousedown", e => e.stopImmediatePropagation())
        closeEl.addEventListener("click", e => {
            e.preventDefault()
            windowObject.close()
        })

        return windowObject
    }

    componentWindow.centerPosition = el => () => {
        const
            bounds = el.getBoundingClientRect(),
            posY = window.pageYOffset + (window.innerHeight - bounds.height)/2,
            posX = window.pageXOffset + (window.innerWidth - bounds.width)/2
        el.style.top = posY+"px"
        el.style.left = posX+"px"
    }

    /*
    * DRAGGING
    * remembers relative position from mouseDown to window position (top left)
    * on mouseMove sets window to that position relative to mouse
    * tinymce editor content is iframe, needs to have own events
    * */
    const initDrag = (editor, windowObject, draggedEl, draggableEl) => {
        const
            mousedown = e => {
                windowObject.isDragging = true
                windowObject.mouseToContainerDragPos.x = parseInt(draggedEl.style.left) - e.pageX
                windowObject.mouseToContainerDragPos.y = parseInt(draggedEl.style.top) - e.pageY
            },
            mouseup = e => {
                windowObject.isDragging = false
                windowObject.mouseToContainerDragPos.x = false
                windowObject.mouseToContainerDragPos.y = false
            },
            mousemove = context => e => {
                if(!windowObject.isDragging)
                    return

                let left = (windowObject.mouseToContainerDragPos.x + e.pageX),
                    top = (windowObject.mouseToContainerDragPos.y + e.pageY)
                if(context.type === "editor") {
                    let coords = plugin.getCoords(context.obj.iframeElement),
                        editorScrollCoords = plugin.getScrollPos(editor.getWin())
                    left += (coords.left - editorScrollCoords.scrollLeft)
                    top += (coords.top - editorScrollCoords.scrollTop)
                }

                draggedEl.style.left = left+"px"
                draggedEl.style.top = top+"px"
            }

        draggableEl.removeEventListener("mousedown", mousedown)
        draggableEl.addEventListener("mousedown", mousedown)

        window.removeEventListener("mouseup", mouseup)
        window.addEventListener("mouseup", mouseup)
        editor.off("mouseup", mouseup)
        editor.on("mouseup", mouseup)

        const
            windowMousemove = mousemove({type: "window", obj: window}),
            editorMousemove = mousemove({type: "editor", obj: editor})
        window.removeEventListener("mousemove", windowMousemove)
        window.addEventListener("mousemove", windowMousemove)
        editor.off("mousemove", editorMousemove)
        editor.on("mousemove", editorMousemove)

        // RESET EVENTS
        windowObject.on("close", () => {
            draggableEl.removeEventListener("mousedown", mousedown)
            window.removeEventListener("mouseup", mouseup)
            window.removeEventListener("mousemove", windowMousemove)
            editor.off("mouseup", mouseup)
            editor.off("mousemove", editorMousemove)
        })
    }
    
    return componentWindow
})(window.tinymceEAPlugin, window.tinymceEAPlugin.component.window, window.tinymceEAPlugin.component)