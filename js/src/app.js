window.tinymceEAPlugin = window.tinymceEAPlugin || {}
window.tinymceEAPlugin.treeview = window.tinymceEAPlugin.treeview || {}
window.tinymceEAPlugin.pluginWindow = window.tinymceEAPlugin.pluginWindow || {}

window.tinymceEAPlugin.component = window.tinymceEAPlugin.component || {}
window.tinymceEAPlugin.component.window = window.tinymceEAPlugin.component.window || {}

window.tinymce = window.tinymce || false

/*
* CORE PLUGIN MODULE
* core "openTreeview" function
* - opens window with treeview for elements insertion
*
* and "actualizeHTMLContent" function
* - actualizes every ea element name in target html node
* - wrapper for "elementsActualization" fn which is also used after tinymce is initilized
*
* plugin initialization loads every needed asset, adds buttons to editor and makes editor content actual
*
* contains global helpers functions
* */

;((plugin, tinymce, treeview, pluginWindow, component, componentWindow) => {

    /*
    * JAVASCRIPT URL SETTINGS
    * */
    const
        appRoot = "",
        pluginRoot = "assets/tinymce/plugins/ea-plugin/"
    plugin.url = {
        eaEditorFile: appRoot+'ea_synchronization/EA_Editor_file.json',
        phpController: pluginRoot+'EAplugin.php',
        contentCSS: pluginRoot+"css/editor-content.css",
        treeviewCSS: pluginRoot+"css/plugin.css"
    }

    /*
     * html class attribute for every type of element in tinymce editor content
     * */
    plugin.elementClass = "ea-plugin-element"

    /*
    * function for actualization of element names for non tinymce editor content
    * */
    plugin.actualizeHTMLContent = (selector = ".ea-content") => {
        let htmlNodes = document.querySelectorAll(selector)
        Array.prototype.forEach.call(htmlNodes, elementsActualization)
    }

    /*
    * PLUGIN INITIALIZATION
    * if tinymce is not initialized
    * don't initialize plugin
    * */
    if(tinymce)
        tinymce.PluginManager.add('ea-plugin', function(editor, url) {

            // ADD TOOLBAR BUTTON
            editor.addButton('ea-plugin', {
                text: 'Vložit prvek z EA',
                icon: false,
                onclick: openTreeview(editor)
            })

            // ADD MENU BUTTON
            editor.addMenuItem('ea-plugin', {
                text: 'Vložit prvek z EA',
                context: 'tools',
                onclick: openTreeview(editor)
            })

            // LOAD CONTENT CSS
            editor.contentCSS = [
                plugin.url.contentCSS
            ]

            // LOAD PLUGIN CSS
            let cssId = 'tinymce-ea-plugin-css'
            if(!document.getElementById(cssId)) {
                let head  = document.getElementsByTagName('head')[0];
                let link  = document.createElement('link');
                link.id   = cssId;
                link.rel  = 'stylesheet';
                link.type = 'text/css';
                link.href = plugin.url.treeviewCSS
                link.media = 'all';
                head.appendChild(link);
            }

            // REFRESHES EA CONTENT
            editor.on('init', e => elementsActualization(editor.dom.getRoot()))

            return { getMetadata: () => ({ title: "EA plugin" }) }
        })

    /*
    * CORE FUNCTION
    * see used functions
    * */
    const openTreeview = editor => () => {
        fetch(plugin.url.eaEditorFile)
            .then(response => {
                if(response.ok)
                    return response.json()
                throw new Error('Network response was not ok.')
            })
            .then(treeview.getDOM)
            .then(pluginWindow.open(editor))
            .then(treeview.init(editor))
            .catch(err => {
                console.error('There has been a problem with your fetch operation: ', err.message);
            })
    }

    /*
    * for every text ea html element in target node actualize its content (name)
    * fetches data from php script which reads them from ea_synchronization directory
    * */
    const elementsActualization = contentNode => {
        let eaElements = contentNode.querySelectorAll("."+plugin.elementClass),
            eaElementsFiltered = Array.prototype.filter.call(eaElements, item => !item.classList.contains("ea-type-image")),
            eaElementGUIDs = Array.prototype.map.call(eaElementsFiltered, item => item.attributes["data-guid"].value)

        let data = new FormData()
        data.append("elementGUIDs", JSON.stringify(eaElementGUIDs))

        fetch(plugin.url.phpController, {
            method: 'post',
            body: data
        })
            .then(response => response.json())
            .then(json => {
                Array.prototype.forEach.call(eaElementsFiltered, item => {
                    let value = json[item.attributes["data-guid"].value]
                    if(value) item.innerHTML = value
                })
            })
    }

    /*
    * UTILS
    * global helpers functions
    * */

    // CREATE HTML TAG WITH ATTRIBUTES
    plugin.tag = (name, attrs = {}) => {
        let el = document.createElement(name.toString())
        !!attrs && Object.keys(attrs).forEach(key => el.setAttribute(key, attrs[key]))
        return el
    }

    // GET POSITION OF HTML ELEMENT
    plugin.getCoords = (elem, parentWindow = window) => {
        var box = elem.getBoundingClientRect();

        var body = parentWindow.document.body;
        var docEl = parentWindow.document.documentElement;

        var scrollTop = parentWindow.pageYOffset || docEl.scrollTop || body.scrollTop;
        var scrollLeft = parentWindow.pageXOffset || docEl.scrollLeft || body.scrollLeft;
        ;({scrollTop, scrollLeft} = plugin.getScrollPos(parentWindow))

        var clientTop = docEl.clientTop || body.clientTop || 0;
        var clientLeft = docEl.clientLeft || body.clientLeft || 0;

        var top  = box.top + scrollTop - clientTop;
        var left = box.left + scrollLeft - clientLeft;

        return { top: Math.round(top), left: Math.round(left) };
    }

    plugin.getScrollPos = targetWindow => ({
        scrollTop: targetWindow.pageYOffset || targetWindow.document.documentElement.scrollTop || targetWindow.document.body.scrollTop,
        scrollLeft: targetWindow.pageXOffset || targetWindow.document.documentElement.scrollLeft || targetWindow.document.body.scrollLeft
    })

    return plugin
})(window.tinymceEAPlugin, window.tinymce, window.tinymceEAPlugin.treeview, window.tinymceEAPlugin.pluginWindow, window.tinymceEAPlugin.component, window.tinymceEAPlugin.component.window)