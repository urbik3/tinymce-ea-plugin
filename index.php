<?php
    require "app.php";
?><!DOCTYPE html>
<html lang="cs">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-language" content="cs" />
    <meta name="googlebot" content="index,follow" />
    <meta name="robots" content="index,follow" />

    <!-- JS -->
    <script src="assets/tinymce/tinymce.js"></script>

    <!-- TITLE -->
    <title>TinyMCE EA plugin</title>
</head>
<body>

<h1>Test 1</h1>

<form method="post">
    <input type="hidden" name="id" value="<?php print $id; ?>" />
    <textarea id="tinymce-editor" style="height: 500px;" name="content"><?php if(@$item) print $item["content"]; else { ?>Hello, World!<?php } ?></textarea>
    <input type="submit" name="save" value="Save" />
</form>

<h2>Test 2</h2>
<p>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</p>
<p>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</p>
<p>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</p>

<div id="window-el">
    <div class="window-container">
        <div class="top-panel">
            <h3 class="title"></h3>
            <a href="#" class="close-btn"></a>
        </div>
        <div class="content"></div>
        <div class="buttons"></div>
    </div>
</div>

<script>
    tinymce.init({
        selector: '#tinymce-editor',
        plugins: 'ea-plugin',
//        toolbar: 'ea-plugin'
    })
</script>

</body>
</html>