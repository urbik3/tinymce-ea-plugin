<?php

require_once "config.php";

$id = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
if(empty($id))
    $id = 1;

if(isset($_POST["save"])) {
    $content = $_POST["content"];
    $id = filter_input(INPUT_POST, "id", FILTER_VALIDATE_INT);

    $stmt = $pdo->prepare("REPLACE INTO `editor` (`id`, `content`) VALUES (:id, :content);");
    $stmt->execute(["content" => $content, "id" => $id]);
}

$stmt = $pdo->prepare("SELECT * FROM `editor` WHERE id = :id;");
$stmt->execute(["id" => $id]);
$item = $stmt->fetch();