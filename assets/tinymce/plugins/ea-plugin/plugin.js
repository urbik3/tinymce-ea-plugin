"use strict";

window.tinymceEAPlugin = window.tinymceEAPlugin || {};
window.tinymceEAPlugin.treeview = window.tinymceEAPlugin.treeview || {};
window.tinymceEAPlugin.pluginWindow = window.tinymceEAPlugin.pluginWindow || {};

window.tinymceEAPlugin.component = window.tinymceEAPlugin.component || {};
window.tinymceEAPlugin.component.window = window.tinymceEAPlugin.component.window || {};

window.tinymce = window.tinymce || false

/*
* CORE PLUGIN MODULE
* core "openTreeview" function
* - opens window with treeview for elements insertion
*
* and "actualizeHTMLContent" function
* - actualizes every ea element name in target html node
* - wrapper for "elementsActualization" fn which is also used after tinymce is initilized
*
* plugin initialization loads every needed asset, adds buttons to editor and makes editor content actual
*
* contains global helpers functions
* */

;(function (plugin, tinymce, treeview, pluginWindow, component, componentWindow) {

    /*
    * JAVASCRIPT URL SETTINGS
    * */
    var appRoot = "",
        pluginRoot = "assets/tinymce/plugins/ea-plugin/";
    plugin.url = {
        eaEditorFile: appRoot + 'ea_synchronization/EA_Editor_file.json',
        phpController: pluginRoot + 'EAplugin.php',
        contentCSS: pluginRoot + "css/editor-content.css",
        treeviewCSS: pluginRoot + "css/plugin.css"

        /*
         * html class attribute for every type of element in tinymce editor content
         * */
    };plugin.elementClass = "ea-plugin-element";

    /*
    * function for actualization of element names for non tinymce editor content
    * */
    plugin.actualizeHTMLContent = function () {
        var selector = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : ".ea-content";

        var htmlNodes = document.querySelectorAll(selector);
        Array.prototype.forEach.call(htmlNodes, elementsActualization);
    };

    /*
    * PLUGIN INITIALIZATION
    * if tinymce is not initialized
    * don't initialize plugin
    * */
    if (tinymce) tinymce.PluginManager.add('ea-plugin', function (editor, url) {

        // ADD TOOLBAR BUTTON
        editor.addButton('ea-plugin', {
            text: 'Vložit prvek z EA',
            icon: false,
            onclick: openTreeview(editor)
        });

        // ADD MENU BUTTON
        editor.addMenuItem('ea-plugin', {
            text: 'Vložit prvek z EA',
            context: 'tools',
            onclick: openTreeview(editor)
        });

        // LOAD CONTENT CSS
        editor.contentCSS = [plugin.url.contentCSS];

        // LOAD PLUGIN CSS
        var cssId = 'tinymce-ea-plugin-css';
        if (!document.getElementById(cssId)) {
            var head = document.getElementsByTagName('head')[0];
            var link = document.createElement('link');
            link.id = cssId;
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = plugin.url.treeviewCSS;
            link.media = 'all';
            head.appendChild(link);
        }

        // REFRESHES EA CONTENT
        editor.on('init', function (e) {
            return elementsActualization(editor.dom.getRoot());
        });

        return { getMetadata: function getMetadata() {
                return { title: "EA plugin" };
            } };
    });

    /*
    * CORE FUNCTION
    * see used functions
    * */
    var openTreeview = function openTreeview(editor) {
        return function () {
            fetch(plugin.url.eaEditorFile).then(function (response) {
                if (response.ok) return response.json();
                throw new Error('Network response was not ok.');
            }).then(treeview.getDOM).then(pluginWindow.open(editor)).then(treeview.init(editor)).catch(function (err) {
                console.error('There has been a problem with your fetch operation: ', err.message);
            });
        };
    };

    /*
    * for every text ea html element in target node actualize its content (name)
    * fetches data from php script which reads them from ea_synchronization directory
    * */
    var elementsActualization = function elementsActualization(contentNode) {
        var eaElements = contentNode.querySelectorAll("." + plugin.elementClass),
            eaElementsFiltered = Array.prototype.filter.call(eaElements, function (item) {
            return !item.classList.contains("ea-type-image");
        }),
            eaElementGUIDs = Array.prototype.map.call(eaElementsFiltered, function (item) {
            return item.attributes["data-guid"].value;
        });

        var data = new FormData();
        data.append("elementGUIDs", JSON.stringify(eaElementGUIDs));

        fetch(plugin.url.phpController, {
            method: 'post',
            body: data
        }).then(function (response) {
            return response.json();
        }).then(function (json) {
            Array.prototype.forEach.call(eaElementsFiltered, function (item) {
                var value = json[item.attributes["data-guid"].value];
                if (value) item.innerHTML = value;
            });
        });
    };

    /*
    * UTILS
    * global helpers functions
    * */

    // CREATE HTML TAG WITH ATTRIBUTES
    plugin.tag = function (name) {
        var attrs = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        var el = document.createElement(name.toString());
        !!attrs && Object.keys(attrs).forEach(function (key) {
            return el.setAttribute(key, attrs[key]);
        });
        return el;
    };

    // GET POSITION OF HTML ELEMENT
    plugin.getCoords = function (elem) {
        var parentWindow = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : window;

        var box = elem.getBoundingClientRect();

        var body = parentWindow.document.body;
        var docEl = parentWindow.document.documentElement;

        var scrollTop = parentWindow.pageYOffset || docEl.scrollTop || body.scrollTop;
        var scrollLeft = parentWindow.pageXOffset || docEl.scrollLeft || body.scrollLeft;
        ;
        var _plugin$getScrollPos = plugin.getScrollPos(parentWindow);

        scrollTop = _plugin$getScrollPos.scrollTop;
        scrollLeft = _plugin$getScrollPos.scrollLeft;


        var clientTop = docEl.clientTop || body.clientTop || 0;
        var clientLeft = docEl.clientLeft || body.clientLeft || 0;

        var top = box.top + scrollTop - clientTop;
        var left = box.left + scrollLeft - clientLeft;

        return { top: Math.round(top), left: Math.round(left) };
    };

    plugin.getScrollPos = function (targetWindow) {
        return {
            scrollTop: targetWindow.pageYOffset || targetWindow.document.documentElement.scrollTop || targetWindow.document.body.scrollTop,
            scrollLeft: targetWindow.pageXOffset || targetWindow.document.documentElement.scrollLeft || targetWindow.document.body.scrollLeft
        };
    };

    return plugin;
})(window.tinymceEAPlugin, window.tinymce, window.tinymceEAPlugin.treeview, window.tinymceEAPlugin.pluginWindow, window.tinymceEAPlugin.component, window.tinymceEAPlugin.component.window);
"use strict";

window.tinymceEAPlugin = window.tinymceEAPlugin || {};
window.tinymceEAPlugin.component = window.tinymceEAPlugin.component || {};
window.tinymceEAPlugin.component.btn = window.tinymceEAPlugin.component.btn || {}

/*
* BUTTON COMPONENT
* needs to be created then appended to dom
* can be disabled using public fn
* */

;(function (plugin, btn, component) {

    btn.className = "ea-plugin-component-btn";

    btn.create = function (_ref) {
        var _ref$text = _ref.text,
            text = _ref$text === undefined ? false : _ref$text,
            _ref$classes = _ref.classes,
            classes = _ref$classes === undefined ? false : _ref$classes,
            _ref$disabled = _ref.disabled,
            disabled = _ref$disabled === undefined ? false : _ref$disabled,
            _ref$onclick = _ref.onclick,
            onclick = _ref$onclick === undefined ? false : _ref$onclick;

        var el = plugin.tag("button", {
            "class": classes + " " + btn.className
        });

        if (text) el.innerText = text;

        if (onclick) el.addEventListener("click", onclick);

        var btnClass = {
            el: el,
            disabled: function disabled(on) {
                return on ? el.classList.add(btn.className + "--disabled") : el.classList.remove(btn.className + "--disabled");
            },
            appendTo: function appendTo(parent) {
                return parent.appendChild(el);
            }
        };

        if (disabled) btnClass.disabled(true);

        return btnClass;
    };

    return btn;
})(window.tinymceEAPlugin, window.tinymceEAPlugin.component.btn, window.tinymceEAPlugin.component);
"use strict";

window.tinymceEAPlugin = window.tinymceEAPlugin || {};
window.tinymceEAPlugin.component = window.tinymceEAPlugin.component || {};
window.tinymceEAPlugin.component.window = window.tinymceEAPlugin.component.window || {}

/*
* WINDOW COMPONENT
* has top panel, content, bottom buttons panel
* can close, hide, show, has close event, can be centered and dragged by top panel
* see "windowObject"
* */

;(function (plugin, componentWindow, component) {

    componentWindow.className = "ea-plugin-component-window";

    componentWindow.open = function (editor, _ref) {
        var _ref$title = _ref.title,
            title = _ref$title === undefined ? false : _ref$title,
            _ref$content = _ref.content,
            content = _ref$content === undefined ? false : _ref$content,
            _ref$buttons = _ref.buttons,
            buttons = _ref$buttons === undefined ? false : _ref$buttons,
            _ref$height = _ref.height,
            height = _ref$height === undefined ? false : _ref$height,
            _ref$width = _ref.width,
            width = _ref$width === undefined ? false : _ref$width;

        var windowEl = plugin.tag("div", {
            "class": componentWindow.className
        });
        windowEl.innerHTML = document.getElementById("window-el").innerHTML;

        /*
        * WINDOW OBJECT
        * */
        var windowObject = {
            // ATTRS
            el: windowEl,

            // METHODS
            close: function close() {
                windowObject.events.close.forEach(function (cb) {
                    return cb();
                });
                windowEl.remove();
            },
            hide: function hide() {
                return windowEl.classList.add(componentWindow.className + "--hidden");
            },
            show: function show() {
                return windowEl.classList.remove(componentWindow.className + "--hidden");
            },
            on: function on(type, fn) {
                if (windowObject.events[type]) windowObject.events[type].push(fn);
            },
            centerPosition: componentWindow.centerPosition(containerEl),

            // INTERNAL
            events: {
                close: []
            },
            isDragging: false,
            mouseToContainerDragPos: { x: false, y: false }

            // WINDOW PARTS
        };var containerEl = windowEl.querySelector(".window-container"),
            panelEl = windowEl.querySelector(".top-panel"),
            titleEl = windowEl.querySelector(".title"),
            closeEl = windowEl.querySelector(".close-btn"),
            contentEl = windowEl.querySelector(".content"),
            buttonsEl = windowEl.querySelector(".buttons");

        // WINDOW CONTENT SETUP
        if (title) titleEl.innerText = title;

        if (content) {
            if (typeof content === "string") {
                contentEl.innerHTML = content;
            } else {
                contentEl.appendChild(content);
            }
        }

        // ADD BUTTONS
        if (buttons) {
            containerEl.classList.add("window-container--has-buttons");

            if (typeof buttons === "string") {
                buttonsEl.innerHTML = buttons;
            } else {
                Array.prototype.forEach.call(buttons, function (btn) {
                    return btn.appendTo(buttonsEl);
                });
            }
        }

        // SET DIMENSIONS
        if (height || width) {
            if (height) {
                contentEl.style.height = height + "px";
                containerEl.style.height = "auto";
            }
            if (width) {
                contentEl.style.width = width + "px";
                containerEl.style.width = "auto";
            }
        }

        initDrag(editor, windowObject, containerEl, panelEl);

        window.document.body.appendChild(windowEl);
        componentWindow.centerPosition(containerEl)();

        closeEl.addEventListener("mousedown", function (e) {
            return e.stopImmediatePropagation();
        });
        closeEl.addEventListener("click", function (e) {
            e.preventDefault();
            windowObject.close();
        });

        return windowObject;
    };

    componentWindow.centerPosition = function (el) {
        return function () {
            var bounds = el.getBoundingClientRect(),
                posY = window.pageYOffset + (window.innerHeight - bounds.height) / 2,
                posX = window.pageXOffset + (window.innerWidth - bounds.width) / 2;
            el.style.top = posY + "px";
            el.style.left = posX + "px";
        };
    };

    /*
    * DRAGGING
    * remembers relative position from mouseDown to window position (top left)
    * on mouseMove sets window to that position relative to mouse
    * tinymce editor content is iframe, needs to have own events
    * */
    var initDrag = function initDrag(editor, windowObject, draggedEl, draggableEl) {
        var mousedown = function mousedown(e) {
            windowObject.isDragging = true;
            windowObject.mouseToContainerDragPos.x = parseInt(draggedEl.style.left) - e.pageX;
            windowObject.mouseToContainerDragPos.y = parseInt(draggedEl.style.top) - e.pageY;
        },
            mouseup = function mouseup(e) {
            windowObject.isDragging = false;
            windowObject.mouseToContainerDragPos.x = false;
            windowObject.mouseToContainerDragPos.y = false;
        },
            mousemove = function mousemove(context) {
            return function (e) {
                if (!windowObject.isDragging) return;

                var left = windowObject.mouseToContainerDragPos.x + e.pageX,
                    top = windowObject.mouseToContainerDragPos.y + e.pageY;
                if (context.type === "editor") {
                    var coords = plugin.getCoords(context.obj.iframeElement),
                        editorScrollCoords = plugin.getScrollPos(editor.getWin());
                    left += coords.left - editorScrollCoords.scrollLeft;
                    top += coords.top - editorScrollCoords.scrollTop;
                }

                draggedEl.style.left = left + "px";
                draggedEl.style.top = top + "px";
            };
        };

        draggableEl.removeEventListener("mousedown", mousedown);
        draggableEl.addEventListener("mousedown", mousedown);

        window.removeEventListener("mouseup", mouseup);
        window.addEventListener("mouseup", mouseup);
        editor.off("mouseup", mouseup);
        editor.on("mouseup", mouseup);

        var windowMousemove = mousemove({ type: "window", obj: window }),
            editorMousemove = mousemove({ type: "editor", obj: editor });
        window.removeEventListener("mousemove", windowMousemove);
        window.addEventListener("mousemove", windowMousemove);
        editor.off("mousemove", editorMousemove);
        editor.on("mousemove", editorMousemove);

        // RESET EVENTS
        windowObject.on("close", function () {
            draggableEl.removeEventListener("mousedown", mousedown);
            window.removeEventListener("mouseup", mouseup);
            window.removeEventListener("mousemove", windowMousemove);
            editor.off("mouseup", mouseup);
            editor.off("mousemove", editorMousemove);
        });
    };

    return componentWindow;
})(window.tinymceEAPlugin, window.tinymceEAPlugin.component.window, window.tinymceEAPlugin.component);
"use strict";

window.tinymceEAPlugin = window.tinymceEAPlugin || {};
window.tinymceEAPlugin.treeview = window.tinymceEAPlugin.treeview || {};
window.tinymceEAPlugin.treeview.dragDrop = window.tinymceEAPlugin.treeview.dragDrop || {};
window.tinymceEAPlugin.pluginWindow = window.tinymceEAPlugin.pluginWindow || {};

window.tinymceEAPlugin.component = window.tinymceEAPlugin.component || {};
window.tinymceEAPlugin.component.btn = window.tinymceEAPlugin.component.btn || {};
window.tinymceEAPlugin.component.window = window.tinymceEAPlugin.component.window || {}

/*
* TREEVIEW DRAGnDROP MODULE
* makes treeview item draggable
* dragging starts after some distance
* droping into tinymce editor content inserts element name OR shows custom window with name/image option
* */

;(function (plugin, dragDrop, treeview, pluginWindow, componentWindow, componentBtn) {

    var itemDraggedPrototype = {
        e: false,
        selectedItem: false,
        draggedElement: false,
        isDragging: false,
        startDrag: false
    };
    var itemDragged = Object.assign({}, itemDraggedPrototype);

    /*
    * MODULE INIT
    * init on window and editor
    * - tinymce editor content is different document (iframe) - needs separate events
    * */
    dragDrop.initDrag = function (editor) {
        // MOUSE MOVE
        var winContext = { type: "window", obj: window },
            windowMoveFn = itemDrag(winContext);
        window.removeEventListener("mousemove", windowMoveFn);
        window.addEventListener("mousemove", windowMoveFn);
        var editorContext = { type: "editor", obj: editor },
            editorMoveFn = itemDrag(editorContext);
        editor.off("mousemove", editorMoveFn);
        editor.on("mousemove", editorMoveFn);

        // MOUSE UP/DROP
        window.removeEventListener("mouseup", resetDrag);
        window.addEventListener("mouseup", resetDrag);
        var dropFn = drop(editor);
        editor.off("mouseup", dropFn);
        editor.on("mouseup", dropFn);

        // RESET EVENTS
        pluginWindow.treeviewWindow.on("close", function () {
            window.removeEventListener("mouseup", resetDrag);
            window.removeEventListener("mousemove", windowMoveFn);
            editor.off("mousemove", editorMoveFn);
            editor.off("mouseup", dropFn);
        });
    };

    /*
    * ITEM INIT
    * */
    dragDrop.itemStartDrag = function (item) {
        return function (e) {
            e.preventDefault();
            itemDragged = Object.assign({}, itemDraggedPrototype, { e: e, startDrag: true, selectedItem: item });
        };
    };

    /*
    * CORE FUNCTIONS
    * itemDrag
    * - dragging item through window and editor
    * - dragging of element starts after some distance
    * drop
    * - dropping item to editor
    * showDialog
    * - after dropping diagram to editor, another custom window appears with insert name/image option
    * resetDrag
    * - default reset all data
    * */
    var itemDrag = function itemDrag(context) {
        return function (e) {
            if (!itemDragged.startDrag) return;

            var distance = Math.max(Math.abs(itemDragged.e.clientX - e.clientX), Math.abs(itemDragged.e.clientY - e.clientY));
            if (!itemDragged.isDragging && distance > 20) {
                if (treeview.selectedItem !== itemDragged.selectedItem) triggerEvent(Array.prototype.find.call(itemDragged.selectedItem.children, function (child) {
                    return child.classList.contains("item-cont");
                }), "click");

                itemDragged.isDragging = true;

                var draggedElement = plugin.tag("div", { id: "tinymce-ea-plugin-dragged-item" });
                draggedElement.innerText = Array.prototype.find.call(itemDragged.selectedItem.children, function (child) {
                    return child.classList.contains("item-cont");
                }).innerText;
                document.body.appendChild(draggedElement);
                itemDragged.draggedElement = document.querySelector("#tinymce-ea-plugin-dragged-item");
            }

            if (!itemDragged.isDragging) return;

            e.preventDefault();

            var x = e.clientX + 10,
                y = e.clientY - 10 - itemDragged.draggedElement.getBoundingClientRect().height,
                coords = void 0;
            if (context.type === "editor") coords = plugin.getCoords(context.obj.iframeElement);else {
                var scrollPos = plugin.getScrollPos(window);
                coords = { left: scrollPos.scrollLeft, top: scrollPos.scrollTop };
            }
            x += coords.left;
            y += coords.top;

            itemDragged.draggedElement.style.left = x + "px";
            itemDragged.draggedElement.style.top = y + "px";
        };
    };

    /*
    * drop item to editor
    * insert item name
    * if item is of type diagram show window with name/image options
    * */
    var drop = function drop(editor) {
        return function (e) {
            if (!itemDragged.isDragging) return;

            // ELEMENT IS DIAGRAM -> SHOW DIALOG
            if (treeview.selectedItem.hasAttributes() && treeview.selectedItem.attributes["data-type"] && treeview.selectedItem.attributes["data-type"].value === "1") {
                resetDrag(e, false);
                window.removeEventListener("mouseup", resetDrag);
                pluginWindow.treeviewWindow.hide();

                showDialog(e, editor)
                // INSERT
                .then(function (isImage) {
                    pluginWindow.insertElement(isImage)();
                    resetDrag(e);
                    pluginWindow.treeviewWindow.show();
                    window.addEventListener("mouseup", resetDrag);
                })
                // CLOSE WINDOW
                .catch(function () {
                    resetDrag(e);
                    pluginWindow.treeviewWindow.show();
                    window.addEventListener("mouseup", resetDrag);
                });
                return;
            }

            // INSERT NON-DIAGRAM ELEMENT
            pluginWindow.insertElement(false)();
            resetDrag(e);
        };
    };

    /*
    * creates new window with image/name option - two buttons
    * */
    var showDialog = function showDialog(e, editor) {
        return new Promise(function (resolve, reject) {
            var insertDiagramName = Object.assign({}, pluginWindow.buttons.diagramName.settings, { disabled: false, onclick: function onclick() {
                    resolve(false);
                    chooseTypeWindow.close();
                }
            }),
                insertDiagramImage = Object.assign({}, pluginWindow.buttons.diagramImage.settings, { disabled: false, onclick: function onclick() {
                    resolve(true);
                    chooseTypeWindow.close();
                }
            });

            var winContent = plugin.tag("div"),
                btnName = componentBtn.create(insertDiagramName),
                btnImage = componentBtn.create(insertDiagramImage);
            btnName.el.style.width = "100%";
            btnImage.el.style.width = "100%";
            btnName.appendTo(winContent);
            btnImage.appendTo(winContent);
            var chooseTypeWindow = componentWindow.open(editor, {
                title: 'Typ vložení',
                content: winContent,
                width: 250,
                height: 60
            });

            chooseTypeWindow.on("close", reject);
        });
    };

    /*
    * default behaviour resets all data
    * resetData = false resets only drag data, selectedItem remains
    * */
    var resetDrag = function resetDrag(e) {
        var resetData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

        if (itemDragged.isDragging) itemDragged.draggedElement.parentNode.removeChild(itemDragged.draggedElement);

        if (resetData) itemDragged = Object.assign({}, itemDraggedPrototype);else {
            itemDragged.startDrag = false;
            itemDragged.isDragging = false;
            itemDragged.draggedElement = false;
        }
    };

    /*
    * UTILS
    * */
    var triggerEvent = function triggerEvent(el, type) {
        if ('createEvent' in document) {
            // modern browsers, IE9+
            var e = document.createEvent('HTMLEvents');
            e.initEvent(type, false, true);
            el.dispatchEvent(e);
        } else {
            // IE 8
            var e = document.createEventObject();
            e.eventType = type;
            el.fireEvent('on' + e.eventType, e);
        }
    };

    return dragDrop;
})(window.tinymceEAPlugin, window.tinymceEAPlugin.treeview.dragDrop, window.tinymceEAPlugin.treeview, window.tinymceEAPlugin.pluginWindow, window.tinymceEAPlugin.component.window, window.tinymceEAPlugin.component.btn);
"use strict";

window.tinymceEAPlugin = window.tinymceEAPlugin || {};
window.tinymceEAPlugin.treeview = window.tinymceEAPlugin.treeview || {};
window.tinymceEAPlugin.treeview.dragDrop = window.tinymceEAPlugin.treeview.dragDrop || {};
window.tinymceEAPlugin.pluginWindow = window.tinymceEAPlugin.pluginWindow || {}

/*
* TREEVIEW COMPONENT
* exposes selectedItem variable (dom node)
* "getDOM" fn creates html content from EA_Editor_file.json
* "init" fn then initializes the component
* */

;(function (plugin, treeview, dragDrop, pluginWindow) {

    var treeviewName = "tinymce-ea-plugin-treeview",
        treeviewSelector = "#" + treeviewName;

    // SELECTED ITEM
    treeview.selectedItem = false;

    /*
    * GET DOM
    * */
    treeview.getDOM = function (json) {
        var insertItem = function insertItem(parent, item) {
            // ITEM
            var hasChildren = !!item.children.length,
                attrs = {
                "data-guid": item.GUID,
                "data-name": item.name,
                "data-type": item.type,
                "class": "item item--type-" + item.type
            };
            attrs.class += hasChildren ? " item--has-children" : "";
            var itemTag = plugin.tag("div", attrs);

            // ITEM CONTENT
            itemTag.appendChild(plugin.tag("div", { "class": "wrap-icon" }));
            var itemCont = plugin.tag("div", { "class": "item-cont" });
            itemCont.appendChild(plugin.tag("span", { "class": "image" }));
            var name = plugin.tag("span", { "class": "name" });
            name.innerHTML = item.name;
            itemCont.appendChild(name);
            itemTag.appendChild(itemCont);

            // CHILDREN
            if (hasChildren) {
                var childrenCont = plugin.tag("div", { "class": "children" });
                item.children.reduce(insertItem, childrenCont);
                itemTag.appendChild(childrenCont);
            }

            parent.appendChild(itemTag);

            return parent;
        };

        var treeviewDOM = json.reduce(insertItem, plugin.tag("div", { "id": treeviewName }));

        return treeviewDOM;
    };

    /*
    * INIT
    * each item is or can be
    * - draggable
    * - selectable
    * - (un)wrapable
    * */
    treeview.init = function (editor) {
        return function () {
            var el = document.querySelector(treeviewSelector),
                find = Array.prototype.find,
                forEach = Array.prototype.forEach,
                unwrapClass = "item--unwrapped",
                selectClass = "item--selected",
                items = el.querySelectorAll(treeviewSelector + " .item");

            dragDrop.initDrag(editor);

            var activateItem = function activateItem(item) {
                var childrenCont = find.call(item.children, function (child) {
                    return child.classList.contains("children");
                }),
                    itemActiveArea = find.call(item.children, function (child) {
                    return child.classList.contains("item-cont");
                }),
                    wrapIcon = item.classList.contains("item--has-children") && find.call(item.children, function (child) {
                    return child.classList.contains("wrap-icon");
                });

                // FIND CHILDREN
                if (childrenCont && childrenCont.children && childrenCont.children.length) forEach.call(childrenCont.children, activateItem);

                // DRAG
                itemActiveArea.addEventListener("mousedown", dragDrop.itemStartDrag(item));

                // SELECT
                itemActiveArea.addEventListener("click", function (e) {
                    var isSelected = item.classList.contains(selectClass);
                    forEach.call(items, function (i) {
                        return i.classList.remove(selectClass);
                    });
                    if (isSelected) {
                        item.classList.remove(selectClass);
                        treeview.selectedItem = false;
                        pluginWindow.itemSelected();
                    } else {
                        item.classList.add(selectClass);
                        treeview.selectedItem = item;
                        pluginWindow.itemSelected();
                    }
                });

                // UNWRAP
                if (wrapIcon) wrapIcon.addEventListener("click", function (e) {
                    if (item.classList.contains(unwrapClass)) {
                        item.classList.remove(unwrapClass);
                        // forEach.call(item.querySelectorAll(treeviewSelector + " .item"), i => i.classList.remove(selectClass))
                    } else item.classList.add(unwrapClass);
                });
            };

            forEach.call(el.children, activateItem);
        };
    };

    return treeview;
})(window.tinymceEAPlugin, window.tinymceEAPlugin.treeview, window.tinymceEAPlugin.treeview.dragDrop, window.tinymceEAPlugin.pluginWindow);
"use strict";

window.tinymceEAPlugin = window.tinymceEAPlugin || {};
window.tinymceEAPlugin.pluginWindow = window.tinymceEAPlugin.pluginWindow || {};
window.tinymceEAPlugin.treeview = window.tinymceEAPlugin.treeview || {};
window.tinymceEAPlugin.component.window = window.tinymceEAPlugin.component.window || {};
window.tinymceEAPlugin.component.btn = window.tinymceEAPlugin.component.btn || {}

/*
* MANAGES WINDOW GUI AND ELEMENT INSERTION
* core window open fn
* "insertElementFactory" curryies into public "pluginWindow.insertElement"
* - insertion can happen from any place of plugin (ex. dragDrop module)
* */

;(function (plugin, treeview, pluginWindow, componentWindow, componentBtn) {

    pluginWindow.buttons = {
        elementName: {
            "class": componentBtn.className + "--element-name",
            settings: undefined,
            btn: undefined
        },
        diagramName: {
            "class": componentBtn.className + "--diagram-name",
            settings: undefined,
            btn: undefined
        },
        diagramImage: {
            "class": componentBtn.className + "--diagram-image",
            settings: undefined,
            btn: undefined
        }

        /*
        * OPEN NEW INSTANCE OF WINDOW
        * uses custom window component and custom btn component
        * */
    };pluginWindow.treeviewWindow = false;

    pluginWindow.open = function (editor) {
        return function (dom) {
            pluginWindow.insertElement = insertElementFactory(editor);
            pluginWindow.treeviewWindow = componentWindow.open(editor, {
                title: "Výběr prvku",
                content: getHTML(dom),
                buttons: createButtons(),
                height: 400,
                width: 620
            });
        };
    };

    var getHTML = function getHTML(dom) {
        var html = document.createElement("div");
        html.appendChild(dom);
        return html.innerHTML;
    };

    var createButtons = function createButtons() {
        pluginWindow.buttons.elementName.settings = {
            text: 'Vložit název elementu',
            classes: componentBtn.className + " " + pluginWindow.buttons.elementName.class,
            disabled: true,
            onclick: pluginWindow.insertElement(false)
        };
        pluginWindow.buttons.elementName.btn = componentBtn.create(pluginWindow.buttons.elementName.settings);

        pluginWindow.buttons.diagramName.settings = {
            text: 'Vložit název diagramu',
            classes: componentBtn.className + " " + pluginWindow.buttons.diagramName.class,
            disabled: true,
            onclick: pluginWindow.insertElement(false)
        };
        pluginWindow.buttons.diagramName.btn = componentBtn.create(pluginWindow.buttons.diagramName.settings);

        pluginWindow.buttons.diagramImage.settings = {
            text: 'Vložit obrázek diagramu',
            classes: componentBtn.className + " " + pluginWindow.buttons.diagramImage.class,
            disabled: true,
            onclick: pluginWindow.insertElement(true)
        };
        pluginWindow.buttons.diagramImage.btn = componentBtn.create(pluginWindow.buttons.diagramImage.settings);

        return [pluginWindow.buttons.elementName.btn, pluginWindow.buttons.diagramName.btn, pluginWindow.buttons.diagramImage.btn];
    };

    /*
     * ELEMENT INSERTION
     * */
    pluginWindow.insertElement = false;

    var insertElementFactory = function insertElementFactory(editor) {
        return function (isImage) {
            return function () {
                var e = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

                if (!treeview.selectedItem) return;

                if (e) e.preventDefault();

                var insertion = void 0,
                    data = new FormData(),
                    elementType = treeview.selectedItem.attributes["data-type"].value,
                    GUID = treeview.selectedItem.attributes["data-guid"].value,
                    name = treeview.selectedItem.attributes["data-name"].value;
                data.append("GUID", GUID);

                // INSERTION TYPE (NAME/IMAGE)
                if (!isImage) {
                    data.append("name", name);
                    data.append("isImage", 0);
                    insertion = "<span class=\"" + plugin.elementClass + " ea-type-" + elementType + "\" data-guid=\"" + GUID + "\" data-type=\"" + elementType + "\">" + name + "</span>";
                    insertion += " ";
                } else {
                    data.append("isImage", 1);
                    insertion = "<div class=\"" + plugin.elementClass + " ea-type-image\" data-guid=\"" + GUID + "\" data-type=\"" + elementType + "\"><img src=\"ea_synchronization/diagrams/" + GUID + ".png\" /></div>";
                    insertion += " ";
                }

                // INSERTION POSITION
                editor.selection.collapse(false);
                var currentNode = editor.selection.getNode();
                if (currentNode.classList.contains(plugin.elementClass)) {
                    var range = document.createRange(),
                        selection = editor.selection;
                    range.setEndAfter(currentNode);
                    selection.setRng(range);
                    insertion = " " + insertion;
                }

                // WRITES FILE NAME/IMAGE PLACEHOLDER TO FILE
                fetch(plugin.url.phpController, {
                    method: 'post',
                    body: data
                }).then(function () {
                    return editor.insertContent(insertion);
                });
            };
        };
    };

    /*
    * ITEM HAS BEEN (UN)SELECTED
    * updates window gui
    * */
    pluginWindow.itemSelected = function () {
        Object.keys(pluginWindow.buttons).map(function (attr) {
            return pluginWindow.buttons[attr].btn;
        }).forEach(function (btn) {
            return btn.disabled(true);
        });

        if (!!treeview.selectedItem) {
            if (treeview.selectedItem.hasAttributes()) {
                var activeButtons = treeview.selectedItem.attributes["data-type"] && treeview.selectedItem.attributes["data-type"].value === "1" ? [pluginWindow.buttons.diagramName, pluginWindow.buttons.diagramImage] : [pluginWindow.buttons.elementName];

                activeButtons.map(function (btn) {
                    return btn.btn;
                }).forEach(function (btn) {
                    return btn.disabled(false);
                });
            }
        }
    };

    return pluginWindow;
})(window.tinymceEAPlugin, window.tinymceEAPlugin.treeview, window.tinymceEAPlugin.pluginWindow, window.tinymceEAPlugin.component.window, window.tinymceEAPlugin.component.btn);
// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        value: function(predicate) {
            // 1. Let O be ? ToObject(this value).
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }

            var o = Object(this);

            // 2. Let len be ? ToLength(? Get(O, "length")).
            var len = o.length >>> 0;

            // 3. If IsCallable(predicate) is false, throw a TypeError exception.
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }

            // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
            var thisArg = arguments[1];

            // 5. Let k be 0.
            var k = 0;

            // 6. Repeat, while k < len
            while (k < len) {
                // a. Let Pk be ! ToString(k).
                // b. Let kValue be ? Get(O, Pk).
                // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
                // d. If testResult is true, return kValue.
                var kValue = o[k];
                if (predicate.call(thisArg, kValue, k, o)) {
                    return kValue;
                }
                // e. Increase k by 1.
                k++;
            }

            // 7. Return undefined.
            return undefined;
        }
    });
}
(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
        typeof define === 'function' && define.amd ? define(factory) :
            (global.Promise = factory());
}(this, (function () { 'use strict';

// Store setTimeout reference so promise-polyfill will be unaffected by
// other code modifying setTimeout (like sinon.useFakeTimers())
    var setTimeoutFunc = setTimeout;

    function noop() {}

// Polyfill for Function.prototype.bind
    function bind(fn, thisArg) {
        return function() {
            fn.apply(thisArg, arguments);
        };
    }

    function handle(self, deferred) {
        while (self._state === 3) {
            self = self._value;
        }
        if (self._state === 0) {
            self._deferreds.push(deferred);
            return;
        }
        self._handled = true;
        Promise._immediateFn(function() {
            var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
            if (cb === null) {
                (self._state === 1 ? resolve : reject)(deferred.promise, self._value);
                return;
            }
            var ret;
            try {
                ret = cb(self._value);
            } catch (e) {
                reject(deferred.promise, e);
                return;
            }
            resolve(deferred.promise, ret);
        });
    }

    function resolve(self, newValue) {
        try {
            // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
            if (newValue === self)
                throw new TypeError('A promise cannot be resolved with itself.');
            if (
                newValue &&
                (typeof newValue === 'object' || typeof newValue === 'function')
            ) {
                var then = newValue.then;
                if (newValue instanceof Promise) {
                    self._state = 3;
                    self._value = newValue;
                    finale(self);
                    return;
                } else if (typeof then === 'function') {
                    doResolve(bind(then, newValue), self);
                    return;
                }
            }
            self._state = 1;
            self._value = newValue;
            finale(self);
        } catch (e) {
            reject(self, e);
        }
    }

    function reject(self, newValue) {
        self._state = 2;
        self._value = newValue;
        finale(self);
    }

    function finale(self) {
        if (self._state === 2 && self._deferreds.length === 0) {
            Promise._immediateFn(function() {
                if (!self._handled) {
                    Promise._unhandledRejectionFn(self._value);
                }
            });
        }

        for (var i = 0, len = self._deferreds.length; i < len; i++) {
            handle(self, self._deferreds[i]);
        }
        self._deferreds = null;
    }

    function Handler(onFulfilled, onRejected, promise) {
        this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
        this.onRejected = typeof onRejected === 'function' ? onRejected : null;
        this.promise = promise;
    }

    /**
     * Take a potentially misbehaving resolver function and make sure
     * onFulfilled and onRejected are only called once.
     *
     * Makes no guarantees about asynchrony.
     */
    function doResolve(fn, self) {
        var done = false;
        try {
            fn(
                function(value) {
                    if (done) return;
                    done = true;
                    resolve(self, value);
                },
                function(reason) {
                    if (done) return;
                    done = true;
                    reject(self, reason);
                }
            );
        } catch (ex) {
            if (done) return;
            done = true;
            reject(self, ex);
        }
    }

    function Promise(fn) {
        if (!(this instanceof Promise))
            throw new TypeError('Promises must be constructed via new');
        if (typeof fn !== 'function') throw new TypeError('not a function');
        this._state = 0;
        this._handled = false;
        this._value = undefined;
        this._deferreds = [];

        doResolve(fn, this);
    }

    var _proto = Promise.prototype;
    _proto.catch = function(onRejected) {
        return this.then(null, onRejected);
    };

    _proto.then = function(onFulfilled, onRejected) {
        var prom = new this.constructor(noop);

        handle(this, new Handler(onFulfilled, onRejected, prom));
        return prom;
    };

    Promise.all = function(arr) {
        return new Promise(function(resolve, reject) {
            if (!arr || typeof arr.length === 'undefined')
                throw new TypeError('Promise.all accepts an array');
            var args = Array.prototype.slice.call(arr);
            if (args.length === 0) return resolve([]);
            var remaining = args.length;

            function res(i, val) {
                try {
                    if (val && (typeof val === 'object' || typeof val === 'function')) {
                        var then = val.then;
                        if (typeof then === 'function') {
                            then.call(
                                val,
                                function(val) {
                                    res(i, val);
                                },
                                reject
                            );
                            return;
                        }
                    }
                    args[i] = val;
                    if (--remaining === 0) {
                        resolve(args);
                    }
                } catch (ex) {
                    reject(ex);
                }
            }

            for (var i = 0; i < args.length; i++) {
                res(i, args[i]);
            }
        });
    };

    Promise.resolve = function(value) {
        if (value && typeof value === 'object' && value.constructor === Promise) {
            return value;
        }

        return new Promise(function(resolve) {
            resolve(value);
        });
    };

    Promise.reject = function(value) {
        return new Promise(function(resolve, reject) {
            reject(value);
        });
    };

    Promise.race = function(values) {
        return new Promise(function(resolve, reject) {
            for (var i = 0, len = values.length; i < len; i++) {
                values[i].then(resolve, reject);
            }
        });
    };

// Use polyfill for setImmediate for performance gains
    Promise._immediateFn =
        (typeof setImmediate === 'function' &&
        function(fn) {
            setImmediate(fn);
        }) ||
        function(fn) {
            setTimeoutFunc(fn, 0);
        };

    Promise._unhandledRejectionFn = function _unhandledRejectionFn(err) {
        if (typeof console !== 'undefined' && console) {
            console.warn('Possible Unhandled Promise Rejection:', err); // eslint-disable-line no-console
        }
    };

    return Promise;

})));

(function(self) {
    'use strict';

    if (self.fetch) {
        return
    }

    var support = {
        searchParams: 'URLSearchParams' in self,
        iterable: 'Symbol' in self && 'iterator' in Symbol,
        blob: 'FileReader' in self && 'Blob' in self && (function() {
            try {
                new Blob()
                return true
            } catch(e) {
                return false
            }
        })(),
        formData: 'FormData' in self,
        arrayBuffer: 'ArrayBuffer' in self
    }

    if (support.arrayBuffer) {
        var viewClasses = [
            '[object Int8Array]',
            '[object Uint8Array]',
            '[object Uint8ClampedArray]',
            '[object Int16Array]',
            '[object Uint16Array]',
            '[object Int32Array]',
            '[object Uint32Array]',
            '[object Float32Array]',
            '[object Float64Array]'
        ]

        var isDataView = function(obj) {
            return obj && DataView.prototype.isPrototypeOf(obj)
        }

        var isArrayBufferView = ArrayBuffer.isView || function(obj) {
                return obj && viewClasses.indexOf(Object.prototype.toString.call(obj)) > -1
            }
    }

    function normalizeName(name) {
        if (typeof name !== 'string') {
            name = String(name)
        }
        if (/[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(name)) {
            throw new TypeError('Invalid character in header field name')
        }
        return name.toLowerCase()
    }

    function normalizeValue(value) {
        if (typeof value !== 'string') {
            value = String(value)
        }
        return value
    }

    // Build a destructive iterator for the value list
    function iteratorFor(items) {
        var iterator = {
            next: function() {
                var value = items.shift()
                return {done: value === undefined, value: value}
            }
        }

        if (support.iterable) {
            iterator[Symbol.iterator] = function() {
                return iterator
            }
        }

        return iterator
    }

    function Headers(headers) {
        this.map = {}

        if (headers instanceof Headers) {
            headers.forEach(function(value, name) {
                this.append(name, value)
            }, this)
        } else if (Array.isArray(headers)) {
            headers.forEach(function(header) {
                this.append(header[0], header[1])
            }, this)
        } else if (headers) {
            Object.getOwnPropertyNames(headers).forEach(function(name) {
                this.append(name, headers[name])
            }, this)
        }
    }

    Headers.prototype.append = function(name, value) {
        name = normalizeName(name)
        value = normalizeValue(value)
        var oldValue = this.map[name]
        this.map[name] = oldValue ? oldValue+','+value : value
    }

    Headers.prototype['delete'] = function(name) {
        delete this.map[normalizeName(name)]
    }

    Headers.prototype.get = function(name) {
        name = normalizeName(name)
        return this.has(name) ? this.map[name] : null
    }

    Headers.prototype.has = function(name) {
        return this.map.hasOwnProperty(normalizeName(name))
    }

    Headers.prototype.set = function(name, value) {
        this.map[normalizeName(name)] = normalizeValue(value)
    }

    Headers.prototype.forEach = function(callback, thisArg) {
        for (var name in this.map) {
            if (this.map.hasOwnProperty(name)) {
                callback.call(thisArg, this.map[name], name, this)
            }
        }
    }

    Headers.prototype.keys = function() {
        var items = []
        this.forEach(function(value, name) { items.push(name) })
        return iteratorFor(items)
    }

    Headers.prototype.values = function() {
        var items = []
        this.forEach(function(value) { items.push(value) })
        return iteratorFor(items)
    }

    Headers.prototype.entries = function() {
        var items = []
        this.forEach(function(value, name) { items.push([name, value]) })
        return iteratorFor(items)
    }

    if (support.iterable) {
        Headers.prototype[Symbol.iterator] = Headers.prototype.entries
    }

    function consumed(body) {
        if (body.bodyUsed) {
            return Promise.reject(new TypeError('Already read'))
        }
        body.bodyUsed = true
    }

    function fileReaderReady(reader) {
        return new Promise(function(resolve, reject) {
            reader.onload = function() {
                resolve(reader.result)
            }
            reader.onerror = function() {
                reject(reader.error)
            }
        })
    }

    function readBlobAsArrayBuffer(blob) {
        var reader = new FileReader()
        var promise = fileReaderReady(reader)
        reader.readAsArrayBuffer(blob)
        return promise
    }

    function readBlobAsText(blob) {
        var reader = new FileReader()
        var promise = fileReaderReady(reader)
        reader.readAsText(blob)
        return promise
    }

    function readArrayBufferAsText(buf) {
        var view = new Uint8Array(buf)
        var chars = new Array(view.length)

        for (var i = 0; i < view.length; i++) {
            chars[i] = String.fromCharCode(view[i])
        }
        return chars.join('')
    }

    function bufferClone(buf) {
        if (buf.slice) {
            return buf.slice(0)
        } else {
            var view = new Uint8Array(buf.byteLength)
            view.set(new Uint8Array(buf))
            return view.buffer
        }
    }

    function Body() {
        this.bodyUsed = false

        this._initBody = function(body) {
            this._bodyInit = body
            if (!body) {
                this._bodyText = ''
            } else if (typeof body === 'string') {
                this._bodyText = body
            } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
                this._bodyBlob = body
            } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
                this._bodyFormData = body
            } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
                this._bodyText = body.toString()
            } else if (support.arrayBuffer && support.blob && isDataView(body)) {
                this._bodyArrayBuffer = bufferClone(body.buffer)
                // IE 10-11 can't handle a DataView body.
                this._bodyInit = new Blob([this._bodyArrayBuffer])
            } else if (support.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(body) || isArrayBufferView(body))) {
                this._bodyArrayBuffer = bufferClone(body)
            } else {
                throw new Error('unsupported BodyInit type')
            }

            if (!this.headers.get('content-type')) {
                if (typeof body === 'string') {
                    this.headers.set('content-type', 'text/plain;charset=UTF-8')
                } else if (this._bodyBlob && this._bodyBlob.type) {
                    this.headers.set('content-type', this._bodyBlob.type)
                } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
                    this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8')
                }
            }
        }

        if (support.blob) {
            this.blob = function() {
                var rejected = consumed(this)
                if (rejected) {
                    return rejected
                }

                if (this._bodyBlob) {
                    return Promise.resolve(this._bodyBlob)
                } else if (this._bodyArrayBuffer) {
                    return Promise.resolve(new Blob([this._bodyArrayBuffer]))
                } else if (this._bodyFormData) {
                    throw new Error('could not read FormData body as blob')
                } else {
                    return Promise.resolve(new Blob([this._bodyText]))
                }
            }

            this.arrayBuffer = function() {
                if (this._bodyArrayBuffer) {
                    return consumed(this) || Promise.resolve(this._bodyArrayBuffer)
                } else {
                    return this.blob().then(readBlobAsArrayBuffer)
                }
            }
        }

        this.text = function() {
            var rejected = consumed(this)
            if (rejected) {
                return rejected
            }

            if (this._bodyBlob) {
                return readBlobAsText(this._bodyBlob)
            } else if (this._bodyArrayBuffer) {
                return Promise.resolve(readArrayBufferAsText(this._bodyArrayBuffer))
            } else if (this._bodyFormData) {
                throw new Error('could not read FormData body as text')
            } else {
                return Promise.resolve(this._bodyText)
            }
        }

        if (support.formData) {
            this.formData = function() {
                return this.text().then(decode)
            }
        }

        this.json = function() {
            return this.text().then(JSON.parse)
        }

        return this
    }

    // HTTP methods whose capitalization should be normalized
    var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT']

    function normalizeMethod(method) {
        var upcased = method.toUpperCase()
        return (methods.indexOf(upcased) > -1) ? upcased : method
    }

    function Request(input, options) {
        options = options || {}
        var body = options.body

        if (input instanceof Request) {
            if (input.bodyUsed) {
                throw new TypeError('Already read')
            }
            this.url = input.url
            this.credentials = input.credentials
            if (!options.headers) {
                this.headers = new Headers(input.headers)
            }
            this.method = input.method
            this.mode = input.mode
            if (!body && input._bodyInit != null) {
                body = input._bodyInit
                input.bodyUsed = true
            }
        } else {
            this.url = String(input)
        }

        this.credentials = options.credentials || this.credentials || 'omit'
        if (options.headers || !this.headers) {
            this.headers = new Headers(options.headers)
        }
        this.method = normalizeMethod(options.method || this.method || 'GET')
        this.mode = options.mode || this.mode || null
        this.referrer = null

        if ((this.method === 'GET' || this.method === 'HEAD') && body) {
            throw new TypeError('Body not allowed for GET or HEAD requests')
        }
        this._initBody(body)
    }

    Request.prototype.clone = function() {
        return new Request(this, { body: this._bodyInit })
    }

    function decode(body) {
        var form = new FormData()
        body.trim().split('&').forEach(function(bytes) {
            if (bytes) {
                var split = bytes.split('=')
                var name = split.shift().replace(/\+/g, ' ')
                var value = split.join('=').replace(/\+/g, ' ')
                form.append(decodeURIComponent(name), decodeURIComponent(value))
            }
        })
        return form
    }

    function parseHeaders(rawHeaders) {
        var headers = new Headers()
        // Replace instances of \r\n and \n followed by at least one space or horizontal tab with a space
        // https://tools.ietf.org/html/rfc7230#section-3.2
        var preProcessedHeaders = rawHeaders.replace(/\r?\n[\t ]+/g, ' ')
        preProcessedHeaders.split(/\r?\n/).forEach(function(line) {
            var parts = line.split(':')
            var key = parts.shift().trim()
            if (key) {
                var value = parts.join(':').trim()
                headers.append(key, value)
            }
        })
        return headers
    }

    Body.call(Request.prototype)

    function Response(bodyInit, options) {
        if (!options) {
            options = {}
        }

        this.type = 'default'
        this.status = options.status === undefined ? 200 : options.status
        this.ok = this.status >= 200 && this.status < 300
        this.statusText = 'statusText' in options ? options.statusText : 'OK'
        this.headers = new Headers(options.headers)
        this.url = options.url || ''
        this._initBody(bodyInit)
    }

    Body.call(Response.prototype)

    Response.prototype.clone = function() {
        return new Response(this._bodyInit, {
            status: this.status,
            statusText: this.statusText,
            headers: new Headers(this.headers),
            url: this.url
        })
    }

    Response.error = function() {
        var response = new Response(null, {status: 0, statusText: ''})
        response.type = 'error'
        return response
    }

    var redirectStatuses = [301, 302, 303, 307, 308]

    Response.redirect = function(url, status) {
        if (redirectStatuses.indexOf(status) === -1) {
            throw new RangeError('Invalid status code')
        }

        return new Response(null, {status: status, headers: {location: url}})
    }

    self.Headers = Headers
    self.Request = Request
    self.Response = Response

    self.fetch = function(input, init) {
        return new Promise(function(resolve, reject) {
            var request = new Request(input, init)
            var xhr = new XMLHttpRequest()

            xhr.onload = function() {
                var options = {
                    status: xhr.status,
                    statusText: xhr.statusText,
                    headers: parseHeaders(xhr.getAllResponseHeaders() || '')
                }
                options.url = 'responseURL' in xhr ? xhr.responseURL : options.headers.get('X-Request-URL')
                var body = 'response' in xhr ? xhr.response : xhr.responseText
                resolve(new Response(body, options))
            }

            xhr.onerror = function() {
                reject(new TypeError('Network request failed'))
            }

            xhr.ontimeout = function() {
                reject(new TypeError('Network request failed'))
            }

            xhr.open(request.method, request.url, true)

            if (request.credentials === 'include') {
                xhr.withCredentials = true
            } else if (request.credentials === 'omit') {
                xhr.withCredentials = false
            }

            if ('responseType' in xhr && support.blob) {
                xhr.responseType = 'blob'
            }

            request.headers.forEach(function(value, name) {
                xhr.setRequestHeader(name, value)
            })

            xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit)
        })
    }
    self.fetch.polyfill = true
})(typeof self !== 'undefined' ? self : this);
// from:https://github.com/jserz/js_piece/blob/master/DOM/ChildNode/remove()/remove().md
(function (arr) {
    arr.forEach(function (item) {
        if (item.hasOwnProperty('remove')) {
            return;
        }
        Object.defineProperty(item, 'remove', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: function remove() {
                if (this.parentNode !== null)
                    this.parentNode.removeChild(this);
            }
        });
    });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);
//# sourceMappingURL=plugin.js.map
