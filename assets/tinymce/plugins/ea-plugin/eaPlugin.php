<?php

/*
 * PHP MODULE FOR EA PLUGIN
 * contains path settings, element insertion and element actualization
 *
 * element insertion is called when ea element is inserted into editor content
 * - adds file to ea_synchronization folder
 *
 * actualization is called on init of tinymce or when is needed for HTML content
 * - actualizes ea element names using files in ea_synchronization folder
 * */

/*
 * PHP PATH SETTINGS
 * */
define("APP_ROOT", $_SERVER["DOCUMENT_ROOT"]."/");
define("EA_SYNCHRO_DIR", APP_ROOT."ea_synchronization/");
define("EA_NAMES_DIR", EA_SYNCHRO_DIR."names/");
define("EA_DIAGRAMS_DIR", EA_SYNCHRO_DIR."diagrams/");

/*
 * ELEMENT INSERTION
 * */
$GUID = filter_input(INPUT_POST, "GUID", FILTER_SANITIZE_STRING);
$name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
$isImage = filter_input(INPUT_POST, "isImage", FILTER_VALIDATE_INT);

if(isset($GUID) and isset($isImage)) {
    if($isImage === 0) { // NAME
        $file = EA_NAMES_DIR.$GUID.".txt";
        if(!file_exists($file)) {
            $handle = fopen($file, 'w') or die("Error creating file");
            fwrite($handle, $name);
            fclose($handle);
        }
    }
    else if ($isImage === 1) { // IMAGE
        $file = EA_DIAGRAMS_DIR.$GUID.".png";
        if(!file_exists($file)) {
            $handle = fopen($file, 'w') or die("Error creating file");
            fwrite($handle, file_get_contents(EA_SYNCHRO_DIR."default-diagram.png"));
            fclose($handle);
        }
    }

    die;
}

/*
 * ELEMENT REFRESH
 * */
if(isset($_POST["elementGUIDs"])) {
    $GUIDs = json_decode($_POST["elementGUIDs"]);
    $result = [];
    foreach($GUIDs as $GUID) {
        $file = EA_NAMES_DIR.$GUID.".txt";
        if(file_exists($file)) {
            $result[$GUID] = file_get_contents($file);
        } else {
            $result[$GUID] = false;
        }
    }
    print json_encode($result);

    die;
}