# EA TinyMCE plugin #

## Requirements

- node.js
- npm
- PHP 5+

## Setup ##

### Minimal setup
- using console run `npm install` in project directory
- then run `gulp` to start developing
- js and less code will be compiled to `assets/tinymce/plugins/ea-plugin/` and `assets/tinymce/plugins/ea-plugin/css` folders respectively
- configure paths in `assets/tinymce/plugins/ea-plugin/EAplugin.php` and `js/src/app.js`
- open `plugin-setup.html` to use plugin
- run `gulp prod` in console to minify css

### Advanced config
- edit config.php to gain access to MySQL database and testing `index.php` and `html.php` files
- run this SQL command in your database
```SQL
CREATE TABLE IF NOT EXISTS `editor` (
  `id` int(10) unsigned NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

ALTER TABLE `editor`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `editor`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
```
- use `index.php` for testing purposes
- `html.php` uses HTML element name actualization feature
- change gulp paths in `gulpfile.js`

## Code review

main files are located in

- js/src
- css/src

other important files are

- assets/tinymce/plugins/ea-plugin/EAplugin.php
- assets/tinymce/plugins/ea-plugin/css/editor-content.css
- ea_synchronization/EA_Editor_file.json

minimal plugin configuration is in plugin-setup.html

### Main modules

#### app.js
Core plugin module

#### window.js
Manages window GUI and element insertion

#### treeview.js
Treeview component

#### EAplugin.php
Files creation and elements actualization