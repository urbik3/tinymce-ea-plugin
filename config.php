<?php

// Start session
session_start();

// DB
$host = '127.0.0.1';
$db   = 'tinymce-ea-plugin';
$user = 'root';
$pass = '';
$charset = 'utf8mb4';

/*
 * DATABASE CONNECTION
 * */
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);