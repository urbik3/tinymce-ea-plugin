<?php
    require "app.php";
?><!DOCTYPE html>
<html lang="cs">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-language" content="cs" />
    <meta name="googlebot" content="index,follow" />
    <meta name="robots" content="index,follow" />

    <!-- CSS -->
    <!--    <link href="css/tinymce-ea-plugin.css?cb=1514732313" rel="stylesheet">-->

    <!-- JS -->
    <script src="js/plugin.js"></script>
    <!--    <script src="js/tinymce-ea-plugin.js?cb=1514733148"></script>-->

    <!-- TITLE -->
    <title>TinyMCE EA plugin</title>
</head>
<body>

<h1>Výsledný text:</h1>
<div class="ea-content">
    <?php if(@$item) print $item["content"]; else { ?>Hello, World!<?php } ?>
</div>

<script>
    tinymceEAPlugin.actualizeHTMLContent() // default: ".ea-content"
    // tinymceEAPlugin.actualizeHTMLContent("#id")
    // tinymceEAPlugin.actualizeHTMLContent(".class")
</script>

</body>
</html>