'use strict'

var fs = require("fs")
var gulp = require('gulp')
var sourcemaps = require('gulp-sourcemaps')
var concat = require('gulp-concat')
var addsrc = require('gulp-add-src')

var babel = require("gulp-babel")
var uglify = require("gulp-uglify")

var less = require('gulp-less')
var postcss = require('gulp-postcss')
var autoprefixer = require('autoprefixer')
var cssnano = require('cssnano')

var paths = {
    layout: "index.php",

    less: "./css/src/app.less",
    less_src: "./css/src/**/*.less",
    css_output_name: "plugin.css",
    css_output_folder: "./css",
    css_plugin_folder: "./assets/tinymce/plugins/ea-plugin/css",

    js_app: "./js/src/app.js",
    js_src: "./js/src/**/*.js",
    js_output_name: "plugin.js",
    js_output_folder: "./js/",
    js_plugin_folder: "./assets/tinymce/plugins/ea-plugin",
    js_libs: ["./js/Array.prototype.find-polyfill.js", "./js/promise-polyfill.js", "./js/fetch-polyfill.js", "./js/ChildNode.remove-polyfill.js"]
}

gulp.task('css_cb', function () {
    var content = fs.readFileSync(paths.layout, 'utf8')
    var hash = '.css?cb=' + Math.round(+new Date()/1000)
    content = content.replace(/\.css\?cb=(\d+)/g, hash)
    return fs.writeFileSync(paths.layout, content)
})

var less_task = function(file, output, prod) {
    let postcss_plugins = [autoprefixer]
    if(prod)
        postcss_plugins.push(cssnano)

    return gulp.src(file)
        .pipe(concat(output))
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(postcss(postcss_plugins))
        .pipe(sourcemaps.write('.') )
        .pipe(gulp.dest(paths.css_output_folder))
        .pipe(gulp.dest(paths.css_plugin_folder))
}

gulp.task('less', ["css_cb"], function () {
    return less_task(paths.less, paths.css_output_name, false)
})

gulp.task('less_prod', ["css_cb"], function () {
    return less_task(paths.less, paths.css_output_name, true)
})

gulp.task('js_cb', function () {
    var content = fs.readFileSync(paths.layout, 'utf8')
    var hash = '.js?cb=' + Math.round(+new Date()/1000)
    content = content.replace(/\.js\?cb=(\d+)/g, hash)
    return fs.writeFileSync(paths.layout, content)
})

gulp.task('js', ["js_cb"], function () {
    return gulp.src([paths.js_app, paths.js_src], {base: 'js/'})
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(addsrc.append(paths.js_libs))
        .pipe(concat(paths.js_output_name))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.js_output_folder))
        .pipe(gulp.dest(paths.js_plugin_folder))
})

gulp.task('js_prod', ['js'], function() {
    gulp.src('./js/app.js')
        .pipe(uglify())
        .pipe(gulp.dest('./js'))
});

// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch(paths.less_src, ['less'])
    gulp.watch(paths.js_src, ['js'])
})

/*
 * GULP MAIN API
 * */

// Production
gulp.task('prod', ['less_prod', 'js_prod'])

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'less', 'js'])